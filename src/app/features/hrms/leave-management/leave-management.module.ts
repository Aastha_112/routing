import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { LeaveManagementRoutingModule } from './leave-management-routing.module';
import { LeaveManagementComponent } from './leave-management.component';
import { LeaveReportComponent } from './leave-report/leave-report.component';
import { LeavesComponent } from './leaves/leaves.component';

@NgModule({
    declarations: [
      LeaveManagementComponent,
      LeavesComponent,
      LeaveReportComponent
    ],
    imports: [
      BrowserModule,
      RouterModule,
      LeaveManagementRoutingModule
    ],
    providers: [],
    bootstrap: [LeaveManagementComponent]
  })
export class LeaveManagementModule { }