import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HrmsComponent } from '../../hrms.component';
import { TimeTrackerComponent } from '../time-tracker.component';
import { CurTimeComponent } from './cur-time/cur-time.component';
import { TimeLogComponent } from './time-log.component';
import { UpdatedTimeComponent } from './updated-time/updated-time.component';

const routes:Routes=[
    {
        path:'hrms',
        component:HrmsComponent,
        children:[
            {
                path:'time-tracker',
                component:TimeTrackerComponent,
                children:[
                    {
                        path:'time-log',
                        component:TimeLogComponent,
                        children:[
                            {
                                path:'current-time',
                                component:CurTimeComponent
                            },
                            {
                                path:'updated-time',
                                component:UpdatedTimeComponent
                            }
                        ]
                    }
                ]
            }
        ]
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class TimeLogRoutingModule { }