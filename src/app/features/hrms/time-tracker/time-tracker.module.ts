import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RequestExtraStaffingComponent } from './request-extra-staffing/request-extra-staffing.component';
import { TimeLogComponent } from './time-log/time-log.component';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { TimeTrackerRoutingModule } from './time-tracker-routing.module';
import { TimeTrackerComponent } from './time-tracker.component';

@NgModule({
    declarations: [
      TimeTrackerComponent,
      TimeLogComponent,
      TimeSheetComponent,
      RequestExtraStaffingComponent
    ],
    imports: [
      BrowserModule,
      TimeTrackerRoutingModule
    ],
    providers: [],
    bootstrap: [TimeTrackerComponent]
  })
  export class TimeTrackerModule { }