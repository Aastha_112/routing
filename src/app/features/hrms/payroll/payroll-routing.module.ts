import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HrmsComponent } from '../hrms.component';
import { PayrollComponent } from './payroll.component';
import { SalarySlipComponent } from './salary-slip/salary-slip.component';

const routes:Routes=[
    {
      path:'hrms',
      component:HrmsComponent,
      children:[
        {
          path:'payroll',
          component:PayrollComponent,
          children:[
            {
              path:'salary-slip',
              component:SalarySlipComponent
            }
          ]
        }
      ]
    }
    
  ]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
export class PayrollRoutingModule { }