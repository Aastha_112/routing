import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HrmsRoutingModule } from './features/hrms/hrms-routing.module';
import { HrmsComponent } from './features/hrms/hrms.component';
import { LeaveManagementRoutingModule } from './features/hrms/leave-management/leave-management-routing.module';
import { LeaveManagementComponent } from './features/hrms/leave-management/leave-management.component';
import { PayrollRoutingModule } from './features/hrms/payroll/payroll-routing.module';
import { PayrollComponent } from './features/hrms/payroll/payroll.component';
import { TimeLogRoutingModule } from './features/hrms/time-tracker/time-log/time-log-routing.module';
import { TimeLogComponent } from './features/hrms/time-tracker/time-log/time-log.component';
import { TimeTrackerRoutingModule } from './features/hrms/time-tracker/time-tracker-routing.module';
import { TimeTrackerComponent } from './features/hrms/time-tracker/time-tracker.component';
import { PsdMgmtComponent } from './features/psd-mgmt/psd-mgmt.component';
import { ToggleComponent } from './features/toggle/toggle.component';
import { HeaderComponent } from './layout/header/header.component';
import { PasswordManagementComponent } from './features/psd-mgmt/password-management/password-management.component';
import { ToggleDashboardComponent } from './features/toggle/toggle-dashboard/toggle-dashboard.component';
import { ToggleReportComponent } from './features/toggle/toggle-report/toggle-report.component';

@NgModule({
  declarations: [
    AppComponent,
    HrmsComponent,
    PsdMgmtComponent,
    ToggleComponent,
    HeaderComponent,
    TimeTrackerComponent,
    LeaveManagementComponent,
    PayrollComponent,
    TimeLogComponent,
    PasswordManagementComponent,
    ToggleDashboardComponent,
    ToggleReportComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HrmsRoutingModule,
    TimeTrackerRoutingModule,
    LeaveManagementRoutingModule,
    PayrollRoutingModule,
    TimeLogRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
