import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatedTimeComponent } from './updated-time.component';

describe('UpdatedTimeComponent', () => {
  let component: UpdatedTimeComponent;
  let fixture: ComponentFixture<UpdatedTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatedTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatedTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
