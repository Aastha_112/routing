import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsdMgmtComponent } from './psd-mgmt.component';

describe('PsdMgmtComponent', () => {
  let component: PsdMgmtComponent;
  let fixture: ComponentFixture<PsdMgmtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsdMgmtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsdMgmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
