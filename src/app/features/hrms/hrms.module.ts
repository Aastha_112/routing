import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HrmsRoutingModule } from './hrms-routing.module';
import { HrmsComponent } from './hrms.component';
import { TimeTrackerComponent } from './time-tracker/time-tracker.component';
import { CompanyPolicyComponent } from './company-policy/company-policy.component';
import { LeaveManagementComponent } from './leave-management/leave-management.component';
import { PayrollComponent } from './payroll/payroll.component';
import { HolidayManagementComponent } from './holiday-management/holiday-management.component';
import { TimeTrackerRoutingModule } from './time-tracker/time-tracker-routing.module';
import { LeavesComponent } from './leave-management/leaves/leaves.component';
import { LeaveReportComponent } from './leave-management/leave-report/leave-report.component';
import { LeaveManagementRoutingModule } from './leave-management/leave-management-routing.module';
import { SalarySlipComponent } from './payroll/salary-slip/salary-slip.component';
import { PayrollRoutingModule } from './payroll/payroll-routing.module';
// import { TimeLogComponent } from './time-tracker/time-log/time-log.component';
// import { TimeSheetComponent } from './time-tracker/time-sheet/time-sheet.component';
// import { RequestExtraStaffingComponent } from './time-tracker/request-extra-staffing/request-extra-staffing.component';



@NgModule({
  declarations: [
    HrmsComponent,
    TimeTrackerComponent,
    CompanyPolicyComponent,
    LeaveManagementComponent,
    PayrollComponent,
    HolidayManagementComponent,
    // LeavesComponent,
    // LeaveReportComponent,
    // SalarySlipComponent,
    // TimeLogComponent,
    // TimeSheetComponent,
    // RequestExtraStaffingComponent,
  ],
  imports: [
    BrowserModule,
    HrmsRoutingModule,
    TimeTrackerRoutingModule,
    LeaveManagementRoutingModule,
    PayrollRoutingModule
  ],
  providers: [],
  bootstrap: [HrmsComponent]
})
export class HrmsModule { }
