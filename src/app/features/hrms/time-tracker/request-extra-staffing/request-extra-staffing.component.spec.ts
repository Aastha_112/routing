import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestExtraStaffingComponent } from './request-extra-staffing.component';

describe('RequestExtraStaffingComponent', () => {
  let component: RequestExtraStaffingComponent;
  let fixture: ComponentFixture<RequestExtraStaffingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestExtraStaffingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestExtraStaffingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
