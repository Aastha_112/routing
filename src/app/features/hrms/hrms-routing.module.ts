import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyPolicyComponent } from './company-policy/company-policy.component';
import { HolidayManagementComponent } from './holiday-management/holiday-management.component';
import { HrmsComponent } from './hrms.component';
import { LeaveManagementComponent } from './leave-management/leave-management.component';
import { PayrollComponent } from './payroll/payroll.component';
import { TimeTrackerComponent } from './time-tracker/time-tracker.component';

const routes: Routes = [
  {
    path:'hrms',
    component:HrmsComponent,
    children:[
      {
        path:'company-policy',
        component:CompanyPolicyComponent
      },
      {
        path:'holiday-management',
        component:HolidayManagementComponent
      },
      {
        path:'leave-management',
        component:LeaveManagementComponent
      },
      {
        path:'payroll',
        component:PayrollComponent
      },
      {
        path:'time-tracker',
        component:TimeTrackerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HrmsRoutingModule { }