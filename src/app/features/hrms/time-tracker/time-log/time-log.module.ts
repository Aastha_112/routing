import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SalarySlipComponent } from '../../payroll/salary-slip/salary-slip.component';
import { TimeTrackerComponent } from '../time-tracker.component';
import { TimeLogRoutingModule } from './time-log-routing.module';
import { TimeLogComponent } from './time-log.component';
import { CurTimeComponent } from './cur-time/cur-time.component';
import { UpdatedTimeComponent } from './updated-time/updated-time.component';

@NgModule({
    declarations: [
      TimeTrackerComponent,
      TimeLogComponent,
      CurTimeComponent,
      UpdatedTimeComponent,
    ],
    imports: [
      BrowserModule,
      TimeLogRoutingModule
    ],
    providers: [],
    bootstrap: [TimeLogComponent]
  })
  export class TimeLogModule { }