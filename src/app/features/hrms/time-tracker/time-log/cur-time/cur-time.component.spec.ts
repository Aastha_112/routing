import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurTimeComponent } from './cur-time.component';

describe('CurTimeComponent', () => {
  let component: CurTimeComponent;
  let fixture: ComponentFixture<CurTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
