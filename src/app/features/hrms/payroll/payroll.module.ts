import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { PayrollRoutingModule } from './payroll-routing.module';
import { PayrollComponent } from './payroll.component';
import { SalarySlipComponent } from './salary-slip/salary-slip.component';

@NgModule({
    declarations: [
      PayrollComponent,
      SalarySlipComponent
    ],
    imports: [
      BrowserModule,
      RouterModule,
      PayrollRoutingModule
    ],
    providers: [],
    bootstrap: [PayrollComponent]
  })
export class PayrollModule { }