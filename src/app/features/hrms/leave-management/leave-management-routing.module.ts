import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HrmsComponent } from '../hrms.component';
import { LeaveManagementComponent } from './leave-management.component';
import { LeaveReportComponent } from './leave-report/leave-report.component';
import { LeavesComponent } from './leaves/leaves.component';

const routes:Routes=[
  {
    path:'hrms',
    component:HrmsComponent,
    children:[
      {
        path:'leave-management',
        component:LeaveManagementComponent,
        children:[
          {
            path:'leaves',
            component:LeavesComponent
          },
          {
            path:'leave-report',
            component:LeaveReportComponent
          }
        ]
      }
    ]
  }
  
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
export class LeaveManagementRoutingModule { }