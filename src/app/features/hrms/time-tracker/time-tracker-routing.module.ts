import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HrmsComponent } from '../hrms.component';
import { RequestExtraStaffingComponent } from './request-extra-staffing/request-extra-staffing.component';
import { TimeLogComponent } from './time-log/time-log.component';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { TimeTrackerComponent } from './time-tracker.component';

const routes:Routes=[
    {
        path:'hrms',
        component:HrmsComponent,
        children:[
            {
                path:'time-tracker',
                component:TimeTrackerComponent,
                children:[
                    {
                        path:'time-log',
                        component:TimeLogComponent
                    },
                    {
                        path:'time-sheet',
                        component:TimeSheetComponent
                    },
                    {
                        path:'request-extra-staffing',
                        component:RequestExtraStaffingComponent
                    }
                ]
            }
        ]
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class TimeTrackerRoutingModule { }