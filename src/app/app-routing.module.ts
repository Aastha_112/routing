import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HrmsComponent } from './features/hrms/hrms.component';
import { PasswordManagementComponent } from './features/psd-mgmt/password-management/password-management.component';
import { PsdMgmtComponent } from './features/psd-mgmt/psd-mgmt.component';
import { ToggleDashboardComponent } from './features/toggle/toggle-dashboard/toggle-dashboard.component';
import { ToggleReportComponent } from './features/toggle/toggle-report/toggle-report.component';
import { ToggleComponent } from './features/toggle/toggle.component';

const routes: Routes = [
  {
    path:'',
    component:HrmsComponent
  },
  {
    path:'hrms',
    // loadChildren: () => import('./features/hrms/hrms.module').then(m => m.HrmsModule),
    component:HrmsComponent
  },
  {
    path:'psd-mgmt',
    component:PsdMgmtComponent
  },
  {
    path:'password-management',
    component:PasswordManagementComponent
  },
  {
    path:'toggle',
    component:ToggleComponent
  },
  {
    path:'toggle-report',
    component:ToggleReportComponent
  },
  {
    path:'toggle-dashboard',
    component:ToggleDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
